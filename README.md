# auth0FetchMock
Mock auth0 fetch requests for testing purpose

# API
```javascript
auth0FetchMock(url, options)
```
# Paramter
## url
the url to mock
## options
```javascript
{
	body: 'file path' // [optinal] the path to the file containing the response content. no default
	status: 200 // [optinal] the response status. no default
	reject: false // [optinal] default false. if true the promise will be rejected. usefull to test if your component handles http exceptions right
}
```
# Usaage
```javascript
// prepare a response for the request to mock
// all auth0fetch request will automatically respond the mocked content instead of really fetching something
auth0FetchMock("/test-request-0", {
	body: "/test/tr0.json",
	status: 200
});
```