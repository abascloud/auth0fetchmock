(function (window) {

	window.__answers = {}

	window.auth0FetchMock = async (url, result) => {
		url = decodeURIComponent(url);
		let mapKey;
		try {
			mapKey = new URL(url).pathname + new URL(url).search;
		} catch (e) {
			let path = window.location.href.split("/");
			path[path.length - 1] = url;
			mapKey = new URL(path.join("/")).pathname + new URL(path.join("/")).search;
			mapKey = mapKey.replace("/test/", "");
		}
		window.__answers[mapKey] = new Promise(async (resolve, reject) => {
			if (typeof result.body == "string") {
				let res = await fetch(result.body)
					.then((response) => {
						return response.body.getReader()
					})
					.then((reader) => {
						return reader.read()
					})
					.then(({ done, value }) => {
						return Object.assign({}, result, {
							body: new TextDecoder("utf-8").decode(value),
							json: () => JSON.parse(new TextDecoder("utf-8").decode(value))
						})

					});
				resolve(res);
			} else {
				resolve(result)
			}
		});
	}

	window.auth0Fetch = (url, options) => {
		url = decodeURIComponent(url);
		return new Promise(async (resolve, reject) => {
			let mapKey;
			try {
				mapKey = new URL(url).pathname + new URL(url).search;
			} catch (e) {
				let path = window.location.href.split("/");
				path[path.length - 1] = url;
				mapKey = new URL(path.join("/")).pathname + new URL(path.join("/")).search;
				mapKey = mapKey.replace("/test/", "");
			}
			if (!(window.__answers[mapKey] instanceof Promise)) {
				let interv = setInterval(() => {
					if (window.__answers[mapKey] instanceof Promise) {
						if (response.hasOwnProperty("reject")) {
							reject(response)
						} else {
							resolve(response)
						}
						clearInterval(interv);
					}
				}, 10);
			} else {
				await window.__answers[mapKey].then(response => {
					if (response.hasOwnProperty("reject")) {
						reject(response)
					} else {
						resolve(response)
					}
				});
			}
		});

	}

})(window)